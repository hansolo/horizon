package eu.hansolo.fx;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;


/**
 * User: hansolo
 * Date: 07.04.14
 * Time: 07:51
 */
public class Demo extends Application {
    private Horizon horizon;
    
    @Override public void init() {
        horizon = new Horizon();
    }

    @Override public void start(Stage stage) {
        StackPane pane = new StackPane();
        pane.getChildren().addAll(horizon);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();
        horizon.setRoll(-22);
        horizon.setPitch(-10);
    }

    @Override public void stop() {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
